import UIKit

protocol ViewControllerProtocol: AnyObject {
  func removeChild(sender: UITableViewCell)
}

class ViewController: UIViewController, ViewControllerProtocol {
  // MARK: - Property
  private var tableView = UITableView()

  private var cleanButton = UIButton()

  private var addChildButton = UIButton()

  private var nameTextField = UITextField()

  private var ageTextField = UITextField()

  private let nameLabelPlaceholder = UILabel(frame: CGRect(x: 10, y: 5, width: 70, height: 11))

  private let ageLabelPlaceholder = UILabel(frame: CGRect(x: 10, y: 5, width: 70, height: 11))

  private let cellID = String(describing: TableViewCell.self)

  private var childrenUser: [Child] = [] {
    didSet {
      if childrenUser.count < 5 {
        addChildButton.isHidden = false
      } else {
        addChildButton.isHidden = true
      }
    }
  }

  @IBOutlet var nameUserTextField: UITextField!
  @IBOutlet var ageUserTextField: UITextField!
  @IBOutlet var stackView: UIStackView!

  // MARK: - LifeCycle
  override func loadView() {
    super.loadView()
    self.tableView = getTableView()
    self.cleanButton = getButton(frame: FrameButton.removeAllChildButton,
                                 title: "Очистить",
                                 colorNormal: .systemRed,
                                 colorHighlighted: .red,
                                 plusOffOn: false,
                                 actionButton: self.clearAllData)
    self.addChildButton = getButton(frame: FrameButton.addChildButton,
                                    title: "Добавить ребенка",
                                    colorNormal: .systemBlue,
                                    colorHighlighted: .blue,
                                    plusOffOn: true,
                                    actionButton: addChild)
    self.textFieldConfig()
    self.view.addSubview(tableView)
    self.view.addSubview(cleanButton)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self

    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    view.addGestureRecognizer(tapGestureRecognizer)
  }


  // MARK: - Button
  private func getButton(frame: CGRect,
                         title: String,
                         colorNormal: UIColor,
                         colorHighlighted: UIColor,
                         plusOffOn: Bool,
                         actionButton: @escaping () -> Void) -> UIButton {
    let button = UIButton(frame: frame)

    button.layer.borderWidth = 1
    button.layer.borderColor = colorNormal.cgColor
    button.layer.cornerRadius = button.frame.size.height / 2
    button.backgroundColor = .white
    button.setTitle(title, for: .normal)
    button.setTitleColor(colorNormal, for: .normal)
    button.setTitleColor(colorHighlighted, for: .highlighted)
    button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
    button.addAction(UIAction(handler: { _ in
      actionButton()
    }), for: .touchUpInside)

    if plusOffOn == true {
    button.setImage(UIImage(systemName: "plus"), for: .normal)
    }

    return button
  }

  // MARK: - TableView
  private func tableViewConfig(tableView: UITableView) {
    let cellNib = UINib(nibName: self.cellID, bundle: nil)
    tableView.register(cellNib, forCellReuseIdentifier: "Cell")
  }

  private func getTableView() -> UITableView {
    let tableView = UITableView()
    let margin: CGFloat = 10
    let window = UIApplication.shared.windows[0]
    let topPadding = window.safeAreaInsets.top
    let bottomPadding = window.safeAreaInsets.bottom + 50
    let stackViewSize = stackView.frame.maxY

    tableView.frame.origin.x = margin
    tableView.frame.origin.y = topPadding + margin + stackViewSize
    tableView.frame.size.width = UIScreen.main.bounds.width - margin * 2
    tableView.frame.size.height = UIScreen.main.bounds.height - tableView.frame.origin.y - margin - bottomPadding
    tableView.separatorStyle = .none
    tableView.rowHeight = 140

    tableViewConfig(tableView: tableView)

    return tableView
  }

  // MARK: - Label
  private func getLabelHeader() -> UILabel {
    let label = UILabel(frame: CGRect(x: 10, y: 0, width: 200, height: 50))
    label.textColor = .black
    return label
  }

  // MARK: - TextFields
  private func textFieldConfig() {
    nameLabelPlaceholder.text = "Имя"
    ageLabelPlaceholder.text = "Возраст"

    nameLabelPlaceholder.textColor = .systemGray3
    ageLabelPlaceholder.textColor = .systemGray3

    nameUserTextField.layer.borderWidth = 1
    ageUserTextField.layer.borderWidth = 1

    nameUserTextField.layer.cornerRadius = 4
    ageUserTextField.layer.cornerRadius = 4
    nameUserTextField.layer.borderColor = UIColor.systemGray3.cgColor
    ageUserTextField.layer.borderColor = UIColor.systemGray3.cgColor

    nameUserTextField.addSubview(nameLabelPlaceholder)
    ageUserTextField.addSubview(ageLabelPlaceholder)
  }

  // MARK: - Methods
  private func clearAllData() {
    let alertController = UIAlertController(title: "Удаление данных", message: "Очисить все поля?", preferredStyle: .actionSheet)
    let dropDataAction = UIAlertAction(title: "Сбросить данные",
                                       style: .destructive) { [weak self] action in
      self?.nameUserTextField.text = ""
      self?.ageUserTextField.text = ""
      self?.childrenUser.removeAll()
      self?.tableView.reloadData()
    }
    let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
    alertController.addAction(dropDataAction)
    alertController.addAction(cancelAction)
    present(alertController, animated: true)
  }

  private func addChild() {
    childrenUser.append(Child(name: nil, age: nil))
    tableView.beginUpdates()
    tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
    tableView.endUpdates()
  }

  func removeChild(sender: UITableViewCell) {
    guard let indexPath = tableView.indexPath(for: sender) else { return }
    childrenUser.remove(at: indexPath.row)
    tableView.beginUpdates()
    tableView.deleteRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
    tableView.endUpdates()
  }

  // MARK: - HideKeyboard
  @objc private func dismissKeyboard() {
    view.endEditing(true)
  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return childrenUser.count
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 45
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = UIView(frame: CGRect(x: 0,
                                          y: 0,
                                          width: tableView.frame.size.width,
                                          height: tableView.frame.size.height))
    let headerLabel = getLabelHeader()

    headerView.backgroundColor = .white
    headerView.addSubview(headerLabel)
    headerLabel.text = "Дети (макс.5)"
    headerView.addSubview(addChildButton)

    return headerView
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableViewCell else {
        return UITableViewCell()
      }

    cell.nameTextField.text = ""
    cell.ageTextField.text = ""
    cell.configTextField()
    cell.delegate = self

    return cell
  }
}
