import UIKit

class TableViewCell: UITableViewCell {

  weak var delegate: ViewControllerProtocol?

  private let nameLabelPlaceholder = UILabel(frame: CGRect(x: 10, y: 5, width: 70, height: 11))
  private let ageLabelPlaceholder = UILabel(frame: CGRect(x: 10, y: 5, width: 70, height: 11))

  @IBOutlet var removeChild: UIButton!
  @IBOutlet var nameTextField: UITextField!
  @IBOutlet var ageTextField: UITextField!
  @IBOutlet var stackViewTextFields: UIStackView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  @IBAction func nameFextFieldEnter(_ sender: UITextField) {
  }
  @IBAction func ageTextFieldEnter(_ sender: UITextField) {
  }
  @IBAction func removeChildTapped(_ sender: UIButton) {
    delegate?.removeChild(sender: self)
  }

  func configTextField() {
    nameLabelPlaceholder.text = "Имя"
    nameLabelPlaceholder.textColor = .systemGray3
    ageLabelPlaceholder.text = "Возраст"
    ageLabelPlaceholder.textColor = .systemGray3
    nameTextField.addSubview(nameLabelPlaceholder)
    ageTextField.addSubview(ageLabelPlaceholder)

    nameTextField.layer.borderWidth = 1
    nameTextField.layer.cornerRadius = 4
    nameTextField.layer.borderColor = UIColor.systemGray3.cgColor

    ageTextField.layer.borderWidth = 1
    ageTextField.layer.cornerRadius = 4
    ageTextField.layer.borderColor = UIColor.systemGray3.cgColor
  }
}
