import UIKit

enum FrameButton {
  static let removeAllChildButton = CGRect(x: UIScreen.main.bounds.width / 4,
                                           y: UIScreen.main.bounds.maxY - 60,
                                           width: UIScreen.main.bounds.width / 2,
                                           height: 40)
  static let addChildButton = CGRect(x: UIScreen.main.bounds.width / 2 - 20,
                                     y: 0,
                                     width: UIScreen.main.bounds.width / 2,
                                     height: 40)
}
